package com.znat.lessons.estimate.app.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nathan on 2014-07-16.
 */
public class Estimate {

    private List<Item> items;

    public Estimate(){
        items = new ArrayList<Item>();
    }

    public double getTotal(){
        double total = 0d;
        for (Item item: items)
            total += item.getItemSubTotal();
        return total;
    }

    public Item getItemAt(int position){
        return items.get(position);
    }

    public void add(Item item){
        items.add(item);
    }

    public void add(){
        items.add(new Item());
    }

    public void insert(Item item, int position){
        items.add(position,item);
    }

    public void remove(Item item){
        items.remove(item);
    }

    public int getItemCount(){
        return items.size();
    }

    public static class Item {
        private String itemName;
        private String itemUnitName;
        private double itemUnitPrice;
        private double itemQuantity;

        public Item(){}

        public Item(String itemName, double itemUnitPrice, String itemUnitName, double itemQuantity) {
            this.itemName = itemName;
            this.itemUnitPrice = itemUnitPrice;
            this.itemUnitName = itemUnitName;
            this.itemQuantity = itemQuantity;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public double getItemUnitPrice() {
            return itemUnitPrice;
        }

        public void setItemUnitPrice(double itemUnitPrice) {
            this.itemUnitPrice = itemUnitPrice;
        }

        public double getItemQuantity() {
            return itemQuantity;
        }

        public void setItemQuantity(double itemQuantity) {
            this.itemQuantity = itemQuantity;
        }

        public String getItemUnitName() {
            return itemUnitName;
        }

        public void setItemUnitName(String itemUnitName) {
            this.itemUnitName = itemUnitName;
        }

        public double getItemSubTotal(){
            return itemQuantity * itemUnitPrice;
        }
    }
}
