package com.znat.lessons.estimate.app;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.mobeta.android.dslv.DragSortController;
import com.mobeta.android.dslv.DragSortListView;
import com.znat.lessons.estimate.app.model.Estimate;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by nathanzylbersztejn on 2014-07-14.
 */
public class EstimateFragment extends ListFragment {
    EstimateAdapter adapter;



    private ListView mDslv;
    private DragSortController mController;

    public int dragStartMode = DragSortController.ON_LONG_PRESS;
    public boolean removeEnabled = true;
    public int removeMode = DragSortController.FLING_REMOVE;
    public boolean sortEnabled = true;
    public boolean dragEnabled = true;

    protected int getLayout() {
        // this DSLV xml declaration does not call for the use
        // of the default DragSortController; therefore,
        // DSLVFragment has a buildController() method.
        return R.layout.dslv_fragment_main;
    }

    public static EstimateFragment newInstance(int headers, int footers) {
        EstimateFragment f = new EstimateFragment();

        Bundle args = new Bundle();
        args.putInt("headers", headers);
        args.putInt("footers", footers);
        f.setArguments(args);

        return f;
    }

    public DragSortController getController() {
        return mController;
    }



    public static class EstimateAdapter extends BaseAdapter{

        Context context;
        Estimate estimate;

        public EstimateAdapter(Context context, Estimate estimate) {
            this.context = context;
            this.estimate = estimate;
        }

        @Override
        public int getCount() {
            return estimate.getItemCount();
        }

        @Override
        public Object getItem(int position) {
           return estimate.getItemAt(position);
        }

        public void remove(Estimate.Item item){
            estimate.remove(item);
            notifyDataSetChanged();
        }

        public void insert(Estimate.Item item, int position){
            estimate.insert(item, position);
            notifyDataSetChanged();
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Estimate.Item e = estimate.getItemAt(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.item_unit, null, false);
                ViewHolder vh = new ViewHolder(convertView);
                convertView.setTag(vh);
            }
            final ViewHolder vh = (ViewHolder) convertView.getTag();

            setEstimateItemValues(e, vh);
            vh.itemQuantity.requestFocusFromTouch();
            vh.itemQuantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void afterTextChanged(Editable s) {
                    e.setItemQuantity(Double.valueOf(s.toString()));
                    setEstimateItemValues(e,vh);
                }
            });

            return convertView;
        }

        private void setEstimateItemValues(Estimate.Item e, ViewHolder vh) {
            vh.itemName.setText(e.getItemName());
            vh.itemUnitName.setText(e.getItemUnitName());
            vh.itemQuantity.setText(String.valueOf(e.getItemQuantity()));
            vh.itemUnitPrice.setText(String.valueOf(e.getItemUnitPrice()));
            vh.itemSubTotal.setText(String.valueOf(e.getItemSubTotal()));

        }

        public static class ViewHolder {
            @InjectView(R.id.itemName) EditText itemName;
            @InjectView(R.id.itemQuantity) EditText itemQuantity;
            @InjectView(R.id.itemUnitName) EditText itemUnitName;
            @InjectView(R.id.itemUnitPrice) EditText itemUnitPrice;
            @InjectView(R.id.itemSubTotal) TextView itemSubTotal;

            public ViewHolder(View v){
                ButterKnife.inject(this,v);
            }
        }
    }

    /**
     * Called in onCreateView. Override this to provide a custom
     * DragSortController.
     */
    public DragSortController buildController(DragSortListView dslv) {
        // defaults are
        //   dragStartMode = onDown
        //   removeMode = flingRight
        DragSortController controller = new DragSortController(dslv);
        controller.setDragHandleId(R.id.cellBackground);
        controller.setClickRemoveId(R.id.click_remove);
        controller.setRemoveEnabled(removeEnabled);
        controller.setSortEnabled(sortEnabled);
        controller.setDragInitMode(dragStartMode);
        controller.setRemoveMode(removeMode);
        controller.setBackgroundColor(Color.WHITE);
        return controller;
    }


    /** Called when the activity is first created. */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mDslv = (ListView) inflater.inflate(getLayout(), container, false);

//        mController = buildController(mDslv);
//        mDslv.setFloatViewManager(mController);
        mDslv.setOnTouchListener(mController);
//        mDslv.setDragEnabled(dragEnabled);

        Estimate estimate = new Estimate();
        estimate.add(new Estimate.Item("Farine",2.13d,"kg",3));
        estimate.add(new Estimate.Item("Oeufs",0.54,"pièce",4));
        estimate.add(new Estimate.Item("Beurre",12,"kg",0.25));
        estimate.add(new Estimate.Item("Sucre",3.06,"kg",0.35));

        adapter = new EstimateAdapter(getActivity(),estimate);
        mDslv.setAdapter(adapter);
        return mDslv;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDslv = (ListView) getListView();
//        mDslv.setDropListener(onDrop);
//        mDslv.setRemoveListener(onRemove);
    }

    private DragSortListView.DropListener onDrop = new DragSortListView.DropListener() {
        @Override
        public void drop(int from, int to) {
        if (from != to) {
            Estimate.Item item = (Estimate.Item)adapter.getItem(from);
            adapter.remove(item);
            adapter.insert(item, to);
        }
        }
    };

    private DragSortListView.RemoveListener onRemove = new DragSortListView.RemoveListener() {
        @Override
        public void remove(int which) {
            adapter.remove((Estimate.Item)adapter.getItem(which));
        }
    };

}
