package ypg.com.utils.network;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.util.Base64;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * Volley adapter for JSON requests that will be parsed into Java objects by Gson.
 */
public class GsonRequest<T> extends Request<T> {
    protected Gson gson;
    private final Map<String, String> mHeaders = new HashMap<String, String>();
    private String body;
    private Type mType;
    protected Class<T> mToClass;
    private final ResponseHandler<T> mResponseHandler;
    private static RequestQueue requestQueue;


    /**
     * Make a request and return a parsed object from JSON with default error handling.
     * If you must deserialize into a generic type, use the other constructor {@link #GsonRequest(int, String, java.lang.reflect.Type, ypg.com.utils.network.GsonRequest.ResponseHandler)}
     *
     * @param method Volley.Method of the request
     * @param url URL of the request to make
     * @param toClass Relevant class object, for Gson's reflection
     * @param responseHandler Handle Success and Error responses
     */
    public GsonRequest(int method, String url, Class toClass, final ResponseHandler<T> responseHandler) {
        super(method, url, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseHandler.onFailure(error);
            }
        });
        this.mToClass = toClass;
        this.mResponseHandler = responseHandler;
        this.body = null;
    }

    /**
     *
     * Make a request and return a parsed object from JSON with default error handling.
     * Should be used when you need to deserialize into a generic type.
     *
     * @param method          Volley.Method of the request
     * @param url             URL of the request to make
     * @param type            ex: new TypeToken<ApiGenericResponse<FlyerDealer>>() {}.getType();
     * @param responseHandler Handle Success and Error responses
     */
    public GsonRequest(int method, String url, Type type, ResponseHandler<T> responseHandler) {
        this(method, url, null, responseHandler);
        mType = type;
    }

    /**
     * Set a custom Gson object
     * @param gson
     * @return
     */
    public GsonRequest<T> withGson(Gson gson){
        this.gson = gson;
        return this;
    }

    /**
     * Set a body (POST)
     * @param body
     * @return
     */
    public GsonRequest<T> setBody(String body){
        this.body = body;
        return this;
    }

    /**
     * Set a basic authentication
     * @param login
     * @param password
     * @return
     */
    public GsonRequest<T> setBasicAuthentication(String login, String password){
        String credentials = login + ":" + password;
        String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        mHeaders.put("Authorization", "Basic " + base64EncodedCredentials);
        return this;
    }

    /**
     * Add a header
     * @param key
     * @param value
     * @return
     */
    public GsonRequest<T> addHeader(String key, String value){
        mHeaders.put(key, value);
        return this;
    }

    /**
     * Add several headers
     * @param headers
     * @return
     */
    public GsonRequest<T> addHeaders(Map<String,String> headers){
        mHeaders.putAll(headers);
        return this;
    }


    /**
     * Execute the request
     */
    public void execute(){
        if (gson == null)
            gson = new Gson();
        getRequestQueue().add(this);
    }

    public static void init(Context context){
        if (context instanceof Activity || context instanceof Service)
            throw new IllegalStateException("Init with application context to avoid leaks");
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaders.size() > 0 ? mHeaders : super.getHeaders();
    }

    @Override
    public byte[] getBody() {
        return body.getBytes(Charset.forName("utf-8"));
    }

    @Override
    public String getBodyContentType() {
        return "application/json; charset=utf-8";
    }

    @Override
    protected void deliverResponse(T response) {
        mResponseHandler.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            //Depending on the constructor used we supply a Class or a Type
            final T result = gson.fromJson(json, mToClass != null ? mToClass : mType);
            return Response.success(result, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    public static RequestQueue getRequestQueue() {
        if (requestQueue == null)
            throw new IllegalStateException("No request queue initialized. Make sure you call init() before firing requests.");

        return requestQueue;
    }

    /**
     * Created by alexd on 2014-07-02.
     */
    public static interface ResponseHandler<T> {
        public void onResponse(T responseObject);
        public void onFailure(Throwable error);
    }
}