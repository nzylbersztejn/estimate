/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ypg.com.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

/**
 * A convenience class for all display related information such as:
 * <ul>
 *     <li>Get the screen dimensions in px or dp</li>
 *     <li>Easy conversions from dp to px and px to dp</li>
 *     <li>Distinguish phones and tablets</li>
 *     <li>Get status bar and action bar height</li>
 * </ul>
 * <h3>Usage</h3>
 * Initialize it in your Application's onCreate() with the default orientation. The default orientation defines which side of the device will be the width or height...
 *  * <pre>
 *     {@code
 *          DisplayInfo.init(this,OrientationMode.PORTRAIT) // this being a context and you can change the default orientation.
 *     }
 * </pre>
 */
public class DisplayInfo {
	public static final String TAG = DisplayInfo.class.getSimpleName();
	private static int screenDensity, screenWidth, screenHeight, actionBarHeight, deviceType;
	public static final int TYPE_PHONE = 0;
	public static final int TYPE_TABLET = 1;
    public static boolean initialized;
	public enum OrientationMode{PORTRAIT,LANDSCAPE};

    /**
     * Initialize the DisplayInfo
     * @param context
     * @param mode
     */
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static void init(Context context, OrientationMode mode) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		screenDensity = metrics.densityDpi;
		if (Build.VERSION.SDK_INT >= 13) {
			Point displaySize = new Point();
			display.getSize(displaySize);
			screenWidth = mode == OrientationMode.LANDSCAPE ? Math.max(displaySize.x, displaySize.y) : Math.min(displaySize.x, displaySize.y);
			screenHeight = mode == OrientationMode.LANDSCAPE ? Math.min(displaySize.x, displaySize.y) : Math.max(displaySize.x, displaySize.y);
		} else {
			screenWidth = mode == OrientationMode.LANDSCAPE ? Math.max(display.getWidth(), display.getHeight()) : Math.min(display.getWidth(), display.getHeight());
			screenHeight = mode == OrientationMode.LANDSCAPE ? Math.min(display.getWidth(), display.getHeight()) : Math.max(display.getWidth(), display.getHeight());
		}
		
		//Action bar
		TypedValue typeValue = new TypedValue();
		context.getTheme().resolveAttribute(android.R.attr.actionBarSize, typeValue, true);
		actionBarHeight = TypedValue.complexToDimensionPixelSize(typeValue.data,context.getResources().getDisplayMetrics());
		
		//Device type
		int orientation = context.getResources().getConfiguration().orientation;
		int rotation = display.getRotation();
		if (orientation == Configuration.ORIENTATION_PORTRAIT && rotation == Surface.ROTATION_0 
				|| orientation == Configuration.ORIENTATION_PORTRAIT && rotation == Surface.ROTATION_180
				|| orientation == Configuration.ORIENTATION_LANDSCAPE && rotation == Surface.ROTATION_90
				|| orientation == Configuration.ORIENTATION_LANDSCAPE && rotation == Surface.ROTATION_270){
			deviceType = TYPE_PHONE;
//			Log.d(TAG, "PHONE");
//			Toast.makeText(context, "PHONE", Toast.LENGTH_SHORT);
		}
		else {
			deviceType = TYPE_TABLET;
//			Log.d(TAG, "TABLET");
//			Toast.makeText(context, "TABLET", Toast.LENGTH_SHORT);
		}
        initialized = true;
	}


    private static void throwNotInitializedException(){
        throw new IllegalStateException("DisplayInfo not initialized. Make sure you call init()");
    }

    /**
     * Get the screen density in absolute value (160, 240,...)
     * @return
     */
	public static int getScreenDensity() {
        if (!initialized)
            throwNotInitializedException();
		return screenDensity;
	}

    /**
     * Get the screen density ratio, which is the screen density returned by {@link #getScreenDensity()} divided by the standard medium density 160
     * @return
     */
	public static float getScreenDensityRatio() {
        if (!initialized)
            throwNotInitializedException();
		return screenDensity / 160f;
	}

    /**
     * Get screen width. Note that it depends on the orientation provided in the initialization method {@link #init(android.content.Context, ypg.com.utils.DisplayInfo.OrientationMode)}
     * @return
     */
	public static int getScreenWidth() {
        if (!initialized)
            throwNotInitializedException();
		return screenWidth;
	}

    /**
     * Get the screen width in density pixels
     * @return
     */
	public static int getScreenWidthInDp() {
        if (!initialized)
            throwNotInitializedException();
		return (int)(screenWidth / getScreenDensityRatio());
	}

    /**
     * Get screen height. Note that it depends on the orientation provided in the initialization method {@link #init(android.content.Context, ypg.com.utils.DisplayInfo.OrientationMode)}
     * @return
     */
	public static int getScreenHeight() {
        if (!initialized)
            throwNotInitializedException();
		return screenHeight;
	}

    /**
     * Get the screen height in density pixels
     * @return
     */
	public static int getScreenHeightInDp() {
        if (!initialized)
            throwNotInitializedException();
		return (int)(screenHeight / getScreenDensityRatio());
	}

    /**
     * Convert density pixels to pixels
     * @param dp
     * @return
     */
	public static int dpToPx(int dp) {
        if (!initialized)
            throwNotInitializedException();
		return (int) (dp * getScreenDensityRatio());
	}

    /**
     * Convert pixels to density pixels
     * @param px
     * @return
     */
	public static int pxToDp(int px) {
        if (!initialized)
            throwNotInitializedException();
		return (int) (px / getScreenDensityRatio());
	}

    /**
     * Get the Action Bar height in pixels
     * @return
     */
	public static int getActionBarHeight(){
        if (!initialized)
            throwNotInitializedException();
		return actionBarHeight;
	}

    /**
     * Returns the status bar height (top of the screen) in pixels
     * @param context
     * @return
     */
    public static int getStatusBarHeight(Activity context) {
        if (!initialized)
            throwNotInitializedException();
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * Get the device type (PHONE or TABLET)
     * @return
     */
    public static int getDeviceType(){
        if (!initialized)
            throwNotInitializedException();
		return deviceType;
	}

    /**
     * Get the current orientation
     * @param context
     * @return
     */
	public static int getOrientation(Context context){
        if (!initialized)
            throwNotInitializedException();
		return context.getResources().getConfiguration().orientation;
	}


}
