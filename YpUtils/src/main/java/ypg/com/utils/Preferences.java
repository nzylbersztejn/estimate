/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ypg.com.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;


/**
 * A convenient wrapper around system preferences that allows to store/retrieve a preference with a single line of code.
 */
public class Preferences {
	public static final String MAIN_PREFERENCES_NAME = "preferences";
	public static final String CURRENT_VERSION = "current_version";
	
	public static SharedPreferences getMainSharedPreferences(Context context) {
		return context.getSharedPreferences(MAIN_PREFERENCES_NAME, Context.MODE_PRIVATE);
	}

	public static SharedPreferences getSettingsSharedPreferences(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	/**
	 * Convenience method to get a shared preference
	 * 
	 * @param context
	 * @param the name of the preference
	 * @return
	 */
	public static int getInt(Context context, String name) {
		SharedPreferences prefs = getMainSharedPreferences(context);
		return prefs.getInt(name, 0);
	}

	/**
	 * Convenience method to get a shared preference
	 * 
	 * @param context
	 * @param the name of the preference
	 * @return
	 */
	public static long getLong(Context context, String name) {
		SharedPreferences prefs = getMainSharedPreferences(context);
		return prefs.getLong(name, 0);
	}

	/**
	 * Convenience method to get a shared preference
	 * 
	 * @param context
	 * @param the name of the preference
	 * @return
	 */
	public static boolean getBoolean(Context context, String name) {
		SharedPreferences prefs = getMainSharedPreferences(context);
		return prefs.getBoolean(name, false);
	}

	/**
	 * Convenience method to get a shared preference
	 * 
	 * @param context
	 * @param the name of the preference
	 * @return
	 */
	public static String getString(Context context, String name) {
		SharedPreferences prefs = getMainSharedPreferences(context);
		return prefs.getString(name, null);
	}

	/**
	 * Convenience method to set a shared preference
	 * 
	 * @param context
	 * @param the name of the preference
	 * @param value
	 */
	public static void setInt(Context context, String name, int value) {
		SharedPreferences prefs = getMainSharedPreferences(context);
		Editor editor = prefs.edit();
		editor.putInt(name, value);
		editor.commit();
	}

	/**
	 * Convenience method to set a shared preference
	 * 
	 * @param context
	 * @param the name of the preference
	 * @param value
	 */
	public static void setLong(Context context, String name, long value) {
		SharedPreferences prefs = getMainSharedPreferences(context);
		Editor editor = prefs.edit();
		editor.putLong(name, value);
		editor.commit();
	}

	/**
	 * Convenience method to set a shared preference
	 * 
	 * @param context
	 * @param the name of the preference
	 * @param value
	 */
	public static void setBoolean(Context context, String name, boolean value) {
		SharedPreferences prefs = getMainSharedPreferences(context);
		Editor editor = prefs.edit();
		editor.putBoolean(name, value);
		editor.commit();
	}

	/**
	 * Convenience method to set a shared preference
	 * 
	 * @param context
	 * @param the name of the preference
	 * @param value
	 */
	public static void setString(Context context, String name, String value) {
		SharedPreferences prefs = getMainSharedPreferences(context);
		Editor editor = prefs.edit();
		editor.putString(name, value);
		editor.commit();
	}

    /**
     * Verify if a preference exists
     * @param context
     * @param name
     * @return
     */
    public static boolean exists(Context context, String name){
        SharedPreferences prefs = getMainSharedPreferences(context);
        return prefs.contains(name);
    }

    /**
     * Removes a preference
     * @param context
     * @param name
     */
    public static void remove(Context context, String name){
        SharedPreferences prefs = getMainSharedPreferences(context);
        Editor editor = prefs.edit();
        editor.remove(name);
        editor.commit();
    }


}
